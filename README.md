# binary_document_Backend

- Implement a Create and Read endpoint for accepting the binary files POSTed from the front-end.
- Save the files in the local storage or in MongoDB (See BSON).
- Implement the code using MVC or the DDD architecture.

# Setting App on local machine
- Do a git clone: git@gitlab.com:asegaismail/binary_document_backend.git

# Running application without docker
- run command: npm i <to install dependences>
- run command: npm start <to load application>
- after app is up and running you can access application via: *** http://localhost:8081/ ***

# Running application via Docker container
- run command: docker build -f Dockerfile -t uploaddocumentbackend .
- run command: docker run -it -p 3005:8081 uploaddocumentbackend
- after container is up and running you can access application via: *** http://localhost:3005/ ***

# Needed enviroment
- Node @14 >
- npm @6 >