// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

// Requiring dependencies
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

// Api
const API_Routes = require('./views/api')

// Initilising app
const app = express();

// Declaring application access port
const port = 8081;

// Handle form data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true,
}))

//Enabling CORS
app.use(cors({
    origin: '*',
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));

// Passing API routes to be used by app
app.use('/', API_Routes)

// Handling non-existant route / path
app.get('*', (req, res) => {
    res.send('404! Invalid Request')
});

//Creating a server and listening port
app.listen(process.env.PORT || port, '0.0.0.0', () => {
    console.log(`Binary Upload App listening on port ${port}`)
})

module.exports = app;