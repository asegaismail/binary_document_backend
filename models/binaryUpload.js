// * @Author: Ismail Debele Asega 
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

const mongoose = require('mongoose');

const kycSchema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    country: String,
},{timestamps: true,});

module.exports = mongoose.model('KycDocuments', kycSchema);