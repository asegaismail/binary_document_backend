// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

// DB Client
const Grid = require("gridfs-stream");
const mongoose = require("mongoose");

const db = mongoose.connection;

let gfs;

db.once('open', () => {
    gfs = Grid(db.db, mongoose.mongo);
    gfs.collection('KycUploads');
})

// Schema
const KYCDocuments = require('../models/binaryUpload');

// Upload Document
const uploadDocument = async (req, res) => {
    try {
        const kycDocumentUpload = new KYCDocuments({
            first_name: 'Ismail',
            last_name: 'Asega',
            country: 'Uganda'
        });
        await kycDocumentUpload.save()
        res.status(200).json({ message: 'File Uploaded Successfully' })
    } catch (err) {
        res.status(500).json(err)
    }
}

// Get all documents
const getAllDocuments = async (req, res) => {
    try {
        gfs.files.find().toArray((err, files) => {
            if (!files || files.length === 0) {
                return res.status(200).json({
                    status: 'Failed',
                    message: 'No KYC Documents Available'
                })
            } else return res.status(200).json({
                status: 'Success',
                files
            })
        })
    } catch (err) {
        console.log(err)
    }
}

module.exports = {
    uploadDocument,
    getAllDocuments
}