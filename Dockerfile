
FROM node:14-alpine

# Creating app directory
WORKDIR /usr/src/binaryBackendApp

# Copying both package.json AND package-lock.json
COPY package*.json ./

# Installing app dependencies
RUN npm install && npm -g install nodemon

# Bundling app source (Copy all files to enviroment)
COPY . .

# Exposing Application Port
EXPOSE 8081

# Command to run the application
CMD [ "nodemon", "app.js" ]