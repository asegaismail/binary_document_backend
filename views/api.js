// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

const express = require('express');
const multer = require('multer');
const { GridFsStorage } = require('multer-gridfs-storage');
const router = express.Router();

const client = require('../db/connection');

const BinaryUploadController = require('../controllers/binaryUploadController')

// Creating storage engine
const storage = new GridFsStorage({
    db: client,
    file: (req, file) => {
        return new Promise((resolve, reject) => {
                const filename = file.originalname;
                const fileInfo = {
                    filename: filename,
                    bucketName: 'KycUploads',
                };
                resolve(fileInfo);
        });
    }
});

const upload = multer({ storage }).single('File');

// Get all uploaded documents
router.get('/kycDocuments', BinaryUploadController.getAllDocuments)

//Upload a new document
router.post('/uploadDocument', upload, BinaryUploadController.uploadDocument);

module.exports = router