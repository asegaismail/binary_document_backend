// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

const mongoose = require("mongoose");

const connectionString = "mongodb+srv://ismail:admin123@cluster0.lxblakp.mongodb.net/kycDocumentsDb?retryWrites=true&w=majority";
const client = mongoose.connect(connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

module.exports = client 